Poka� dziennik 
%s - kliknij, aby uzyska� szczeg�owe informacje
Dodatkowe informacje o bohaterze
Poka� informacje o wysokim morale
Poka� informacje o neutralnym morale
Poka� informacje o niskim morale
Poka� informacje o wysokim szcz�ciu
Poka� informacje o neutralnym szcz�ciu
Poka� informacje o niskim szcz�ciu
Poka� informacje o posiadanym do�wiadczeniu
Wybierz - %s
Wolne miejsce
Przenie� oddzia� - %s
Zamie� pozycje jednostek - %s i %s
Poka� zakl�cia
%s - poka� informacje
%s, %s - zwolnij bohatera
Wyjd� z ekranu bohatera
Ekran bohatera
Po��cz oddzia� - %s
Podziel oddzia� - %s
Poka� informacje o poziomie %s umiej�tno�ci %s
Poka� informacje o punktach magii
Ustaw oddzia�y w lu�nej formacji
Ustaw oddzia�y w zwartej formacji
Nie u�ywaj formacji taktycznej
U�ywaj formacji taktycznej
Poka� informacje o specjalnych zdolno�ciach
Kliknij tutaj, aby pozby� si� tego bohatera
Uaktywnienie lu�nej formacji oznacza, �e przed rozpocz�ciem bitwy oddzia�y zostan� rozmieszczone w lu�nym szyku na du�ym obszarze pola bitwy.
Uaktywnienie zwartej formacji oznacza, �e przed rozpocz�ciem bitwy oddzia�y zostan� rozmieszczone w zwartym szyku na ma�ym obszarze pola bitwy.
"Uaktywnienie formacji taktycznej pozwoli bohaterowi na rozmieszczenie swoich wojsk przed rozpocz�ciem bitwy (bohater musi posiada� umiej�tno�� ""Taktyka"")."
Kliknij, aby podzieli� ten oddzia� na dwie grupy.
