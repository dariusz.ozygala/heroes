Latarnia morska	Latarnia morska zwi�ksza zasi�g wszystkich twoich okr�t�w.
Bastion gryf�w	Bastion gryf�w powi�ksza populacj� gryf�w o 3 tygodniowo.
Bastion gryf�w	Bastion gryf�w powi�ksza populacj� gryf�w kr�lewskich o 3 tygodniowo.
Stocznia	Stocznia pozwala na budow� okr�t�w.
Stajnie	Stajnie dodaj� zwi�kszaj� zasi�g ruchu ka�dego bohatera, kt�ry je odwiedzi.
Bractwo Miecza	Podczas obl�enia Bractwo Miecza zwi�ksza morale garnizonu o +2.



Kolos	Obecno�� Kolosa zwi�ksza cotygodniowy przyrost podstawowej populacji wszystkich stworze� o 50%, generuje codziennie 5000 sztuk z�ota, oraz zwi�ksza morale wszystkich bohater�w o +2.
Magazyn zasob�w	Magazyn zasob�w dostarcza codziennie 1 s�g drewna i 1 w�z rudy.
Magiczny staw	Magiczny staw dostarcza cotygodniowo losow� liczb� bogactw.
Gildia g�rnicza	Gildia g�rnicza powi�ksza populacj� krasnolud�w o 4 tygodniowo.
Gildia g�rnicza	Gildia g�rnicza powi�ksza populacj� krasnoludzkich wojownik�w o 4 tygodniowo.

Fontanna szcz�cia	Fontanna szcz�cia podczas obl�enia zwi�ksza szcz�cie bohatera dowodz�cego garnizonem o +2.
Skarbiec	Skarbiec powi�ksza zasoby z�ota generuj�c na pocz�tku tygodnia dodatkowe 10% z�ota (od posiadanej ilo�ci).

Zagajnik	Zagajnik zwi�ksza populacj� drzewc�w o 2 tygodniowo.
Zagajnik	Zagajnik zwi�ksza populacj� ent�w o 2 tygodniowo.
Duchowy stra�nik	Obecno�� duchowego stra�nika zwi�ksza cotygodniowy przyrost podstawowej populacji wszystkich stworze� o 50%, generuje codziennie 5000 sztuk z�ota oraz zwi�ksza morale wszystkich bohater�w o +2.
Magazyn zasob�w	Magazyn zasob�w dostarcza codziennie 1 kryszta�.
Sprzedawca artefakt�w	Za skromn� op�at� mo�esz od tego sprzedawcy artefakt�w zakupi� artefakt.
Skrzyd�a Stw�rcy	Skrzyd�a stw�rcy powi�kszaj� populacj� gargulc�w o 4 tygodniowo
Skrzyd�a Stw�rcy	Skrzyd�a stw�rcy powi�kszaj� populacj� granitowych gargulc�w o 4 tygodniowo

Wie�a stra�nicza	Wie�a stra�nicza pozwala na szczeg�ow� obserwacj� okolic miasta.
Biblioteka	Biblioteka dostarcza dodatkowych zakl�� dla twojej Gildii mag�w.
�ciana Wiedzy	�ciana Wiedzy o +1 zwi�ksza poziom wiedzy ka�dego bohatera, kt�ry j� odwiedzi.


Podniebny galeon	Podniebny galeon zwi�ksza cotygodniowy przyrost podstawowej populacji wszystkich stworze� o 50%, dostarcza codziennie 5000 sztuk z�ota, odkrywa ca�� map� oraz zwi�ksza na czas obl�enia wiedz� bohatera o +15.
Magazyn zasob�w	Magazyn zasob�w dostarcza codziennie 1 klejnot.

Narodzisko	Narodzisko powi�ksza populacj� chochlik�w o 8 tygodniowo.
Narodzisko	Narodzisko powi�ksza populacj� chowa�c�w o 8 tygodniowo.

Chmury Siarki	Chmury Siarki zwi�kszaj� o +2 moc bohatera dowodz�cego garnizonem podczas obl�enia.
Wrota wymiar�w	Pozwalaj� na przenoszenie jednostek do innych miast, w kt�rych tak�e znajduj� si� wrota wymiar�w.
Bractwo Ognia	Bractwo Ognia zwi�ksza moc ka�dego odwiedzaj�cego je bohatera o +1.
Klatki	Klatki zwi�kszaj� populacj� ogar�w piekie� o 3 tygodniowo.
Klatki	Klatki zwi�kszaj� populacj� cerber�w o 3 tygodniowo.
B�stwo ognia	Obecno�� b�stwa ognia zwi�ksza cotygodniowy przyrost populacji wszystkich stworze� o 50% oraz generuje codziennie 5000 sztuk z�ota . Dodatkowo ka�dy nast�pny tydzie� b�dzie tygodniem chochlika.
Magazyn zasob�w	Magazyn zasob�w dostarcza codziennie 1 uncj� rt�ci.
Ca�un ciemno�ci	Ca�un ciemno�ci pokrywa miasto ukrywaj�c je przed innymi graczami.
Rozkopane groby	Rozkopane groby dostarczaj� cotygodniowo 6 szkielet�w.
Rozkopane groby	Rozkopane groby dostarczaj� cotygodniowo 6 ko�ciei.
Stocznia	Stocznia pozwala na zakup okr�t�w.
Nekromatron	Nekromatron powi�ksza o 10% umiej�tno�� nekromancji u wszystkich twoich nekromant�w.
Szkieletornia	Szkieletornia pozwala przekszta�ci� dowolne jednostki w szkielety.



Wi�zienie Dusz	Wi�zienie Dusz zwi�ksza cotygodniowy przyrost podstawowej populacji wszystkich stworze� o 50%, dostarcza codziennie 5000 sztuk z�ota oraz zwi�ksza o 20% umiej�tno�� nekromancji u wszystkich bohater�w, kt�rzy j� posiadaj�.
Magazyn zasob�w	Magazyn zasob�w dostarcza codziennie 1 s�g drewna i 1 w�z rudy.
Sprzedawca artefakt�w	Za skromn� op�at� mo�esz od tego sprzedawcy artefakt�w zakupi� artefakt.
Czarci kr�g	Czarci kr�g dostarcza cotygodniowo 7 troglodyt�w.
Czarci kr�g	Czarci kr�g dostarcza cotygodniowo 7 piekielnych troglodyt�w.

Wir magii	Wir magii czasowo podwaja liczb� punkt�w magii ka�dego bohatera, kt�ry odwiedzi miasto.
Portal przyzwania	Z pomoc� portalu przyzwania mo�na rekrutowa� stworzenia z odleg�ych zabudowa�.
Akademia Wojny	Akademia Wojny przekazuje bohaterowi now� umiej�tno�� oraz 1000 punkt�w do�wiadczenia.


Stra�nik Ziemi	Obecno�� Stra�nika Ziemi zwi�ksza cotygodniowy przyrost podstawowej populacji wszystkich stworze� o 50%, daje codziennie 5000 sztuk z�ota oraz zwi�ksza podczas obl�enia moc bohatera dowodz�cego garnizonem o +12.
Magazyn zasob�w	Magazyn zasob�w dostarcza codziennie 1 uncj� siarki.
Podziemny tunel	Podziemny tunel pozwala na ucieczk� garnizonu z obl�onego miasta.
Chata	Chata zwi�ksza cotygodniow� produkcj�  goblin�w o 8.
Chata	Chata zwi�ksza cotygodniow� produkcj� hobgoblin�w o 8.

Gildia najemnik�w	Gildia najemnik�w pozwala na wymian� jednostek na zasoby.
Du�y warsztat	Pozwala na wyprodukowanie balisty.
Sale Walhalli	Sale Walhalli zwi�kszaj� o +1 zdolno�� ataku ka�dego bohatera, kt�ry odwiedzi miasto.


Pomnik wodza	Pomnik wodza zwi�ksza cotygodniowy przyrost podstawowej populacji wszystkich stworze� o 50%, generuje codziennie 5000 sztuk z�ota oraz zwi�ksza podczas obl�enia zdolno�� ataku bohatera dowodz�cego garnizonem o +20.
Magazyn zasob�w	Magazyn zasob�w dostarcza codziennie 1 s�g drewna i 1 w�z rudy.
Sanktuarium	Sanktuarium dodaje 1 punkt obrony dla ka�dego bohatera, kt�ry odwiedzi miasto.
Kwatery oficerskie	Kwatery oficerskie zwi�kszaj� produkcj� gnolli o 6 tygodniowo.
Kwatery oficerskie	Kwatery oficerskie zwi�kszaj� produkcj� gnolli maruder�w o 6 tygodniowo.
Stocznia	Stocznia pozwala na zakup okr�t�w.
Wilcze do�y	Wilcze do�y dodaj� +2 do obrony bohaterowi dowodz�cemu garnizonem podczas obl�enia.
Obelisk Krwi	Obelisk Krwi dodaje +2 do ataku bohaterowi dowodz�cemu garnizonem podczas obl�enia miasta.



Mi�so�erna ro�lina	Obecno�� wielkiej, mi�so�ernej ro�liny powoduje zwi�kszenie cotygodniowego przyrostu podstawowej populacji wszystkich stworze� o 50%, generowanie codziennie 5000 sztuk z�ota, a podczas obl�enia podnosi zdolno�ci ataku i obrony bohatera dowodz�cego garnizonem o +10.
Magazyn zasob�w	Magazyn zasob�w dostarcza codziennie 1 s�g drewna i 1 w�z rudy.
Sprzedawca artefakt�w	Za skromn� op�at� mo�esz od tego sprzedawcy artefakt�w zakupi� artefakt.
Ogr�d �ycia	Ten budynek zwi�ksza cotygodniow� produkcj� nimf o 10 tygodniowo.
Ogr�d �ycia	Ten budynek zwi�ksza cotygodniow� produkcj� rusa�ek o 10 tygodniowo.
Stocznia	Stocznia pozwala na zakup okr�t�w.
Magiczna uczelnia	Za odpowiedni� oplat� mo�esz tu pobiera� nauk� w dowolnej dziedzinie magii.




Cudowna t�cza	Cudowna t�cza zwi�ksza cotygodniowy przyrost podstawowej populacji wszystkich stworze� o 50%, generuje codziennie 5000 sztuk z�ota oraz udost�pnia wszystkie zakl�cia w twojej gildii mag�w.
Magazyn zasob�w	Magazyn zasob�w dostarcza codziennie 1 uncj� rt�ci.
Sprzedawca artefakt�w	Za skromn� op�at� mo�esz od tego sprzedawcy artefakt�w zakupi� artefakt.
Budynek hordy poziom 1	Ten budynek zwi�ksza cotygodniow� produkcj� stworze� poziomu 1.
Budynek hordy poziom 2	Ten budynek zwi�ksza cotygodniow� produkcj� stworze� poziomu 2.
Stocznia	Stocznia pozwala na zakup okr�t�w.


Budynek hordy poziom 3	Ten budynek zwi�ksza cotygodniow� produkcj� stworze� poziomu 3.
Budynek hordy poziom 4	Ten budynek zwi�ksza cotygodniow� produkcj� stworze� poziomu 4.
Budynek hordy poziom 5	Ten budynek zwi�ksza cotygodniow� produkcj� stworze� poziomu 5.
Graal	Obecno�� Graala zwi�ksza cotygodniowy przyrost podstawowej populacji wszystkich stworze� o 50%, generuje codziennie 5000 sztuk z�ota oraz dodaje specjaln� premi� zale�n� od typu miasta.
Magazyn zasob�w	Magazyn zasob�w dostarcza codziennie dodatkowe zasoby.
