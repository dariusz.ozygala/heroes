Na pocz�tku s�u�by wojskowej Orrin by� szkolony przez jednego z najlepszych taktyk�w erathia�skich. Teraz �ucznicy znajduj�cy si� pod jego rozkazami szybko ucz� si� strzela� do ukrytych za przeszkodami cel�w.
Valeska zdoby�a s�aw� jako mistrzyni celno�ci, kiedy po raz pierwszy s�u�y�a w ertharia�skiej armii. Obecnie nie tylko dowodzi w�asnymi si�ami, ale r�wnie� szkoli swoich kusznik�w. 
Pradziadek Erica by� pierwszym cz�owiekiem w Erathii, kt�ry udomowi� i wytresowa� dzikiego gryfa. Teraz rodzina Erica posiada najwi�ksz� w Erathii hodowl� gryf�w na u�ytek kr�lewskiej armii.
Sylwia kilka lat prowadzi�a �ywot pirata, zanim zda�a sobie spraw� z tego, �e takie �ycie nie jest dla niej. Postanowi�a zacz�� wszystko od pocz�tku, teraz s�u�y Erathii odpieraj�c najazdy tych samych pirat�w, z kt�rymi niegdy� �eglowa�a.
M�wi si�, �e wyjazd lorda Harata z Enroth by� spowodowany wi�zami z nekromanckim kultem, ale jego s�u�ba koronie Erathii by�a tak samo bez skazy, jak wtedy gdy s�u�y� dla Rolanda Ironfista, jeszcze przed Wojn� o Sukcesj�.
Pod��aj�c za przyk�adem kr�lowej Katarzyny, Sorsha do��czy�a do szereg�w erathia�skiej armii. Szybko pokaza�a, �e jest mistrzyni� walki na miecze. Dow�dztwo nad pierwszym oddzia�em przyznano jej kr�tko po przybyciu Kreegan do Erathii.
Christian by� zawsze bardziej pionierem ni� rycerzem. Zanim zdecydowa� si� na wojskow� s�u�b� w Erathii zd��y� odwiedzi� wszystkie zak�tki Enroth. Pomimo �e jest w�drowcem i marzycielem, jego taktyczne umiej�tno�ci siej� postrach na polach bitew ca�ego �wiata.
"Tyris wspina�a si� szybko w rankingach erathia�skiej kawalerii nie tylko z powodu wyj�tkowych umiej�tno�ci je�dzieckich, ale r�wnie� dzi�ki ""sz�stemu zmys�owi"" odpowiadaj�cemu za ofensywn� strategi� i taktyk�. Nigdy nie przegra�a w turnieju rycerskim."
Rion pracowa� jako medyk w erathia�skiej armii. Potwierdzi� on swoje dow�dcze zdolno�ci, i gdy jego kapitan poleg� w walce z hordami Kreegan zwodzi� wroga do czasu a� przyby�y posi�ki.
Pocz�tkowo Adela u�ycza�a swoich umiej�tno�ci jedynie przed bitw�. Je�li nie mog�a przekona� dow�dcy do unikni�cia potyczki, udziela�a b�ogos�awie�stwa �o�nierzom id�cym do walki. Obj�a dow�dztwo nad garnizonem Whitestone.
Jako m�ody cz�owiek Cuthbert para� si� czarn� magi�, ale kiedy �le rzucone zakl�cie doprowadzi�o do �mieci jego �ony, powr�ci� na stron� dobra i ju� nigdy wi�cej nie ogl�da� si� wstecz. Zrozumienie zasad dzia�ania czarnej magii da�o mu bro�, jakiej nie posiada nikt inny.
Z�apana przez sztorm, Adelaide rozbi�a si� u brzeg�w Vori, domu �nie�nych elf�w. Trenowa�a u nich przez oko�o dwadzie�cia lat, a po powrocie do Erathii odkry�a, �e podczas jej nieobecno�ci czas stan�� tam w miejscu.
Przed atakiem Kreegan, Ingham prowadzi� skromny klasztor. On i jego mnisi szybko zostali wcieleni do s�u�by dla erathia�skiej Korony.
Sanya zawsze by�a bardzo zdoln� uczennic�, zawsze zostawiaj�cym w tyle innych student�w. Wydaje si�, �e ma naturalny dar przyswajania czar�w. Czasami wystarczy tylko, by kto� rzuci� przy niej zakl�cie...
Loynis zawsze wierzy� w to, �e fizyczna przemoc nie jest potrzebna, je�li ma si� pod r�k� odpowiedniego maga. Cho� zdumiewaj�ce, udawa�o mu si� post�powa� wed�ug tej zasady, wi�c teraz jest postrzegany jako skuteczny, chocia� bardzo niekonwencjonalny przyw�dca.
Zanim przyznano jej dow�dztwo, przez niekt�rych Caitlin by�a uwa�ana za op�tan� przez z�e moce. Pomimo tych pom�wie� do jej ko�cio�a sp�ywa�y nieprzeliczone dary.
Wyszkolona w szeregach Erathia�skiej milicji, Mephala by�a geniuszem kiedy przychodzi�o do wykorzystania ukszta�towania terenu podczas bitwy. Zrezygnowa�a z oficerskiego stopnia kilka lat temu, przedk�adaj�c cichy spok�j w AvLee nad krz�tanin� w miastach Erathii.
To niezwyk�e widzie� krasnoludzkiego strzelca, w dodatku urodzonego lidera, ale Ufretin mia� po prostu oba te dary we krwi. Nic dziwnego, �e sta� si� symbolem ca�ej zamieszkuj�cej powierzchni� ziemi populacji krasnolud�w.
Wi�kszo�� Erathian ceni�a Jenov� z powodu jej dobrego serca. Jej urzekaj�cy wdzi�k pozwala� jej na zebranie funduszy na wszystko, czego zdecydowa�a si� podj��.
Ryland by� pierwszym (i jak dot�d jedynym) cz�owiekiem, kt�ry zosta� w pe�ni zaakceptowany przez ko�o Starszyzny ent�w jako strzelec z AvLee. Niekt�rzy �artuj�, �e kiedy� by� elfem, zosta� zabity i dzi�ki reinkarnacji wr�ci� w ludzkim ciele.
Osierocony w dzieci�stwie, Thorgrim wychowywa� si� w najbardziej wysuni�tych na p�noc lasach AvLee. W�r�d swoich rodak�w sta� si� s�awny dzi�ki wyj�tkowej odporno�ci na dzia�anie wszelkich rodzaj�w magii.
Linia rodu Iv ora si�ga prawie do Wielkiej Ciszy. Chocia� wychowany na arystokrat�, zawsze by� porywczy i wola� poprowadzi� atak ni� czeka�, a� zrobi� to inni. Najpierw dzia�a�, p�niej my�la�, ale jak dot�d ta metoda sprawdza�a si� znakomicie.
Przez czysty przypadek Clancy odkry�, �e potrafi porozumiewa� si� z Jednoro�cami, ale ta niespotykana umiej�tno�� bardzo mu si� przyda�a, wykorzystywa� j� podczas s�u�by w Elitarnej Milicji AvLee.
Kyrre s�u�y�a jako zwiadowca w pocz�tkowym stadium obrony AvLee przed atakami Kreegan. Talent do orientacji w trudnym terenie pozwoli� jej celuj�co spe�nia� swoje obowi�zki.
Coronius ucz�szcza� na Uniwersytet Erathii przez jeden semestr, zanim zda� sobie spraw� z tego, �e nauczyciele akademiccy byli bardzo przeceniani. Wyjecha� do AvLee, gdzie znalaz� druida, kt�ry potrafi� uczy� na przyk�adach.
Uland sp�dzi� spory kawa�ek �ycia jako polowy medyk zanim wybra�  �ycie druida, ale lekcje, kt�re odebra� s�u��c w wojsku, uczyni�y z niego wybitnego przyw�dc�.
Elleshar jest przypuszczalnie najbardziej b�yskotliwym studentem magii, jakiego kiedykolwiek widzia�a Erathia. Je�li uda mu si� utrzyma� swoje zdolno�ci przez kilka kolejnych dekad, prawdopodobnie zostanie najm�odszym druidem zaakceptowanym przez ko�o starszyzny.
Gem by�a jedn� z najwi�kszych czarodziejek, jakie kiedykolwiek widziano w Enroth. S�u�y�a kr�lowi Rolandowi �elaznej Pi�ci podczas Wojny Secesyjnej. Kr�tko po tym, jak Roland zapewni� sobie tron Enroth, Gem wyjecha�a do Erathii, znajduj�c nowy dom w AvLee.
Malcolm jest jednym z niewielu krasnolud�w, kt�rzy maj� talent do magii. Cz�sto do poznania nowych zakl�� wystarcza mu jedynie bacznie obserwowa� kogo�, kto je w�a�nie rzuca.  Szybko zosta� wcielony w szeregi broni�cych AvLee przed diab�ami z Eeofol.
Melodia mo�e nie nale�y do najlepszych druid�w w AvLee, ale z pewno�ci� jest najwi�ksz� szcz�ciar� spo�r�d nich. Nawet w przypadku mia�d��cej przewagi wroga zdo�a�a odnie�� wspania�e zwyci�stwa. Oddzia�y garn� si� do s�u�by w armii, kt�r� ona dowodzi.
O Alagarze kr��� pog�oski, �e kiedy� mieszka� w�r�d �nie�nych elf�w Vori. Jego w�adza nad �ywio�em wody, a szczeg�lnie nad lodem, jest nieprze�cigniona. S�u�y� AvLee jeszcze przed inwazj� Kreegan.
Aeris jest jednym z najlepszych treser�w w ca�ym AvLee. Jest jednym z najbardziej cenionych druid�w na ca�ym �wiecie.
W szkole alchemii Piquedram wi�kszo�� czasu sp�dza� nad r�nego typu ska�ami o pozornie ma�ej warto�ci. Nikogo wi�c nie dziwi, �e w s�u�bie Brakady zdo�a� udoskonali� metody tworzenia gargulc�w. 
Nikt nic nie wie o przesz�o�ci Thane, ale uczy on alchemii w Brakadzie tak d�ugo, �e nawet najstarsi nie pami�taj� czas�w, kiedy go tam jeszcze nie by�o.
Josephine jako pierwszej uda�o o�ywi� si� kamiennego golema. By� to proces znacznie bardziej skomplikowany ni� w przypadku gargulc�w, s�u��cych jej za podstaw� bada�. 
Neela imponuje si�� - jej odporno�� na b�l jest wprost niewiarygodna. Jakim� cudem udaje jej si� wpoi� t� cech� oddzia�om, kt�rymi dowodzi.
Mimo �e Torosar odebra� wykszta�cenie alchemiczne, od zawsze interesowa� si� taktyk� walk i obl�e�. Ksi�gi o wojnie studiowa� cz�ciej  ni� te o alchemii. 
Kr��� s�uchy, �e Fafner zosta� d�innem ponad tysi�c lat temu, kiedy po znalezieniu butelki pewnego d�inna za�yczy� sobie, �e chce by� tak samo pot�ny jak on. �yczenie zosta�o spe�nione i od tamtej pory Fafner s�u�y przyw�dcom Brakady.
Rissie jako pierwszej uda�o si� doprowadzi� do perfekcji sztuk� zamieniania mi�kkich metali w rt��. Chocia� teraz dowodzi w�asn� armi�, sporo wolnego czasu sp�dza eksperymentuj�c nad innymi materia�ami.
Dawno temu Iona udowodni�a, �e jest silniejsza ni� wi�kszo�� istot jej pokroju, ale jej czaruj�ca osobowo�� chroni jej podw�adnych przed onie�mieleniem powodowanym zwykle przez jej si��.
Astral przyby� do Erathii blisko dziesi�� lat temu i szybko zosta� przyj�ty do Gildii mag�w w Brakadzie. W szeregach Gildii jego pot�ga ros�a tak szybko, �e �artowano, i� tylko magia mog�a doprowadzi� go do sukcesu w takim tempie. 
Halon by� bardzo szanowanym bohaterem w Enroth, ale po Wojnie o Sukcesj� by� ju� tak znudzony wszystkim, co go otacza�o, �e wyruszy�  w �wiat. Ostatnio przyj�� stanowisko dow�dcy w szeregach milicji, maj�c nadziej�, �e przyniesie mu ono ciekawe wyzwania.
Kiedy pierwszy raz Serena rzuci�a zakl�cie, ma�o brakowa�o by zgin�a. W�o�y�a w to tak du�o magicznej energii, �e wyczu� j� mag z s�siedniego miasteczka. Odnalaz� j� i zabra� ze sob� do Gildii Mag�w, gdzie nauczy�a si� kontrolowa� swoje umiej�tno�ci.
Daremyth powinna by�a zgin�� ju� dawno temu. Jej wsz�dobylski spos�b �ycia wp�dza� j� tarapaty, kt�rych nikt normalny by nie prze�y�. Ona jednak wychodzi�a z nich bez najmniejszego zadra�ni�cia....
Theodorus jest jednym z najlepszych czarodziei w Brakadzie. W wyniku tego ca�e t�umy domoros�ych mag�w cz�sto staraj� si� zosta� jego uczniami.
Solmyr by� zamkni�ty w butelce d�inna przez ponad tysi�c lat i by� tak wdzi�czny cz�owiekowi, kt�ry go stamt�d uwolni�, �e przysi�g� s�u�y� mu przez ca�� wieczno��. Los sprawi�, �e tym cz�owiekiem by� Gavin Magnus, nie�miertelny w�adca Wy�yn Brakady.
Na pocz�tku Cyra pragn�a uczy� si� magii tylko po to, by czarami sprawia�, i� m�czy�ni b�d� si� w niej zakochiwa�. Jednak zrezygnowa�a z tego marzenia, gdy odkry�a, �e jej prawdziwe magiczne talenty mog� by� wykorzystywane w znacznie bardziej po�yteczny spos�b.
Aine mieszka w Brakadzie d�u�ej ni� ktokolwiek pami�ta. Kr��� plotki, �e jest jedn� z najbogatszych os�b w kraju. Musi gdzie� ukrywa� nieprzebrane skarby, poniewa� jej dobre serce ka�e jej regularnie wyk�ada� olbrzymie sumy na sprawy, w kt�re si� osobi�cie anga�uje.
Przed inwazj� diab��w Fiona by�a treserem zwierz�t cyrkowych we wschodniej Erathii. Szybko udowodni�a naje�d�com, �e jej mo�liwo�ci kontroli nad zwierz�tami obejmuj� r�wnie� agresywne psy i w ten spos�b natychmiast zaakceptowano j� w szeregach wojsk Eeofol.
Rashka jest jednym z najsilniejszych i najbardziej budz�cych strach ze wszystkich ifrit�w. Jego dow�dcze metody polegaj� na onie�mielaniu, jako metodzie motywacji.
Przez kilka lat Marius by�a ma��onk� Calha, w ko�cu rozstali si�, gdy on nie zgodzi� si� na jej pomys� ostatecznego rozwi�zania kwestii tych obrzydliwie dobrych le�nych stworze�.
Kiedy Diab�y przyby�y do Erathii, Ignatius zda� sobie spraw�, �e jego jedyn� nadziej� na przetrwanie by�o przy��czenie si� do nich. Do tej pory udaje mu si� je przekonywa� o swojej przydatno�ci, lecz ca�y czas obawia si�, �e nied�ugo przestanie im by� potrzebny.   
M�wi si�, �e Octavia potrafi skra�� z�oto �pi�cemu smokowi. Ci�gle wydaje bajo�skie sumy na spe�nianie swoich rozlicznych kaprys�w.
Przez kilka lat Calh by� m�em Marius, ale gdy zorientowa� si�, �e ma ona obsesj� na punkcie zniszczenia wszystkich drzewc�w rozeszli si�. Teraz Calh skupi� si� przede wszystkim na kontynuowaniu swojej wspania�ej wojskowej kariery.
Nikt nie zna prawdziwego imienia Pyre, ale kr��� pog�oski o tym, �e kiedy� by�a cz�onkiem humanist�w - stowarzyszenia maj�cego na celu wykorzenienie wszystkich nie przypominaj�cych ludzi mieszka�c�w Erathii. Tylko ona zna powody, dla kt�rych s�u�y diab�om z Eeofol.
Nymus by�a odpowiedzialna za przygotowanie czart�w do ataku na Erathi�. Jej wojskowe zdolno�ci prawie doprowadzi�y Eeofol do ca�kowitego zwyci�stwa, ale uk�ady Kreegan i w�adc�w Podziemi obr�ci�y jej wysi�ki w proch.
Ayden, kt�ry by� szamanem, zaoferowa� swoje us�ugi Kreeganom. Jednocze�nie odkry� w sobie pragnienie, aby pewnego dnia zasi��� na tronie w�asnego kr�lestwa.
Xyron zdaje si� lubi� ogie� bardziej, ni� mo�na by si� tego spodziewa� po ifritach. On po prostu rozkoszuje si� wszystkim, co ma jakikolwiek zwi�zek z ogniem lub law�.
W m�odo�ci Axis wykazywa� wrodzony talent do magii, co  wydaje si� dziwne, poniewa� demony rzadko wykazuj� zdolno�ci w kierunku czegokolwiek, co wymaga my�lenia.
Olema sp�dzi�a ca�e swoje dzieci�stwo w Eeofol, ale kiedy przybyli Kreeganie, nam�wi�a ich by przyj�li j� do s�u�by w charakterze przewodnika i od tamtej pory nie raz ju� dowiod�a swych przyw�dczych zdolno�ci.  
Calid posiada niezwyk�� zdolno�� wyczuwania ukrytych z�� siarki. Wielokrotnie by�a wykorzystywana przez W�adc�w Podziemi do zdobywania siarki dla smok�w.
Ash zyska�a swoje imi� podczas pierwszego ataku na Erathi�. Jej si�y zdo�a�y podst�pnie uwi�zi� kilka tuzin�w elf�w i krasnolud�w w  olbrzymim drewnianym forcie, kt�ry Ash przy pomocy zakl�cia zmiot�a z powierzchni ziemi.
Kiedy Zydar nie dowodzi wojskiem, oddaje si� badaniu nowych sposob�w wzmacniania swoich czar�w. Chocia� nie jest najlepszym magiem, z pewno�ci� mo�na pok�ada� w nim wi�ksze nadzieje ni� w jakimkolwiek innym demonie.
Xarfax s�u�y� w erathia�skiej armii a� do roku 1162, kiedy to zosta� pojmany w czasie walki z Kreeganami. Kreeganie, zdaj�c sobie spraw� z jego pot�gi, przeci�gn�li go na swoj� stron�.
Straker wybra� ciemn� drog� nekromancji, ale wkr�tce odkryto, �e jego umiej�tno�ci walki s� znacznie wi�ksze ni� zdolno�ci magiczne. Teraz s�u�y w armii Deyji, marz�c, �e pewnego dnia do��czy do szereg�w prawdziwych nekromant�w.
Blisko cztery wieki temu Vokial ustanowi� w�asne kr�lestwo, ale z up�ywem czasu by� coraz bardziej znudzony codziennymi obowi�zkami. W ko�cu rzuci� wszystko, by wr�ci� do �ycia awanturnika, kt�re najbardziej mu odpowiada�o. Nale�y do starszyzny wampir�w.
Charyzma Moandora pozosta�a przy nim nawet wtedy, gdy zosta� zamieniony w lisza. Zaskakuj�ce, ale pozwoli�o mu to rekrutowa� i dowodzi� nowymi liszami bardziej efektywnie, ni� jakiemukolwiek innemu bohaterowi.
Charna zosta�a uwiedziona przez pon�tne ciemne moce. Chocia� skromne zdolno�ci magiczne nigdy nie pozwol�  jej zosta� prawdziw� Nekromantk�, jest kim� wi�cej ni� skuteczn� wojowniczk�.
Tamika posiada niezwykle wysokie umiej�tno�ci walki, kt�re pozwoli�y jej zaj�� wp�ywow� pozycj� w si�ach zbrojnych Deyna. Opr�cz tego, �e po prostu dowodzi oddzia�ami, jest r�wnie� odpowiedzialna za szkolenie czarnych rycerzy.
Isra wprawdzie nie jest najpot�niejszym magiem w Deyji, lecz armia pod jej wodz� zdaje si� mie� znacznie wi�cej szkielet�w ni� inne armie.
Rodzina Claviusa zawsze posiada�a du�e wp�ywy polityczne na dworze oraz ogromne w�o�ci. Clavius, jako najstarszy syn, zosta� wys�any do wojska, jak nakazywa�a tradycja. S�u�y ch�tnie i dobrze.
Przez lata Galthran udoskonala� nowe metody tworzenia szkielet�w i dowodzenia nimi, dzi�ki czemu odnosi� sukcesy w walce.
"Septienna cierpi z powodu zjawiska, kt�re przez wielu jest nazywane ""zmarnowan� energi�"". Ze swoich ofiar czerpie znacznie wi�cej energii ni� przekazuje swoim podopiecznym. Niekt�rzy twierdz�, �e reszt� energii kumuluje w sobie, co t�umaczy�oby jej m�ody wygl�d."
Aislinn zawsze wola�a ogl�da� ziemi� z lotu ptaka. Wielu zadziwia �atwo��, z jak� zwraca �ycie zmar�ym.
Sandro studiowa� nekromancj� najpierw pod kuratel� maga, a p�niej lisza, Ethrica. Przew�drowa� niemal ca�e Enroth i Erathi�, a obecnie s�u�y Finneasowi Vilmarowi, przyw�dcy nekromant�w w Deyja.
Kiedy Nimbus by� ma�ym ch�opcem, mia� psa, kt�ry w ko�cu zdech� ze staro�ci. Nimbus odkry�, �e wystarczy z�o�y� w ofierze ma�ego ptaszka, aby wskrzesi� swojego ulubie�ca. Czynno�� t� powtarza� za ka�dym razem, gdy jego pies umiera�. Zanim zda� sobie z tego spraw�, pozna� pierwsze tajniki nekromancji.
Nikt nie zna prawdziwego wieku Thanta. Podobno walczy� za Erathi� podczas Le�nych Wojen, ale pad� wtedy ofiar� wampira w czasie ca�onocnego marszu na Phynaxi�.
Xsi by�a jedn� z kobiet, kt�re wybrano do przemiany w lisza. Zosta�a wybrana poniewa� posiada ona jedyn� w swoim rodzaju odporno�� na fizyczne zniszczenie.
W czasach m�odo�ci Vidomina zapowiada�a si� na wspania�� alchemiczk�, ale musia�a opu�ci� Brakad�, kiedy wysz�o na jaw, �e wykorzystuje swoje zdolno�ci nie do o�ywiania przedmiot�w lecz do o�ywiania istot, kt�re straci�y �ycie.
"Nagash by� pot�nym magiem zanim po�wi�ci� si� ""wiecznemu �yciu"" lisza. Posiada wiele ziem, chocia� wi�kszo�� z nich le�y w granicach opustosza�ego Deyja."
Kiedy Lorelei by�a dzieckiem odesz�a za daleko od domu i zgubi�a si� w sieci jaski�. Znaleziona i wychowana przez harpi� wied�m� zg�osi�a si� na ochotnika do s�u�by u w�adc�w Podziemi na kr�tko przed inwazj� na Erathi�. Nie pami�ta swoich prawdziwych rodzic�w.
Arlach jest jednym z niewielu troglodyt�w, kt�rym powierzono dow�dztwo nad si�ami Nighonu. Wykazuje on niesamowite zdolno�ci w wykorzystywani katapulty.
Dace urodzi� si� w klanie wojownik�w. Pobiera� nauki od mistrz�w, kt�rzy wydawali mu si� starsi ni� sam czas. Jest wyj�tkowym dow�dc�, szczeg�lnie gdy wydaje rozkazy minotaurom.
"Ajit przestudiowa� uwa�nie prace Agara, g�upiego czarodzieja-naukowca z Enroth, kt�ry przypadkiem stworzy� obserwator�w. Ajit teraz nimi dowodzi, pr�buj�c stworzy� ""lepsz� ras�"" na u�ytek bojowy."
Chocia� Damacon by� wy�miewany nawet przez troglodyt�w, potrafi odszuka� z�oto zawsze i wsz�dzie. Dlatego te� nigdy nie sk�pi na �adne cele, ch�tnie widz�c si� na stanowisku dow�dcy.
Gunnar najpierw s�u�y� jako zwiadowca w�adc�w mroku, p�niej jako przewodnik i ochroniarz wysokich rang� urz�dnik�w, w ko�cu przyznano mu dow�dztwo nad si�ami stacjonuj�cymi przy granicach Nighonu. Braki intelektualne nadrabia instynktem.
Synca �y�a kiedy� na l�dzie i chocia� obecnie wiernie s�u�y w�adcom mroku, woli otwarte przestrzenie ni� zat�ch�e i ciasne jaskinie Nighonu.
Bezoki Shakti jest zadziwiaj�co dobrym taktykiem. Jego hordy troglodyt�w s� postrachem ca�ego �wiata, szczeg�lnie w ciemnych lochach Nighonu.
Alamar s�u�y� Archibaldowi Ironfistowi podczas Wojny Secesyjnej, ale wkr�tce po kl�sce opu�ci� Enroth. Osiad� w Nighonie i zacz�� pracowa� w tajnych s�u�bach W�adc�w Podziemi.
Jaeger zd��y� potwierdzi� swoje magiczne zdolno�ci. Twierdzi, �e kluczem do jego sukcesu jest codzienna medytacja.
Mo�na by oczekiwa�, �e minotaur zostanie wojownikiem, ale Malekith w�ada tak siln� magi�, �e �mia�o m�g�by stan�� w zawody z najlepszymi magami �wiata.
Niekt�rzy m�wi�, �e Jeddite widzia�a twarz Zenofex, inni twierdz�, �e nie mo�e to by� prawd� skoro wci�� �yje. Ona sama nigdy nie zaprzecza, ale te� nie potwierdza tym pog�oskom.
Wi�kszo�� magicznej mocy Geon zawdzi�cza umiej�tno�ci wnikania w umys�y innych mag�w oraz czytania ich my�li, kiedy rzucaj� zakl�cia lub przywo�uj� swoj� moc, co pozwala mu szybko uczy� si� nowych zakl��.
W m�odo�ci Deemer o ma�o nie zgin��, poniewa� jego magiczna moc wymkn�a mu si� spod kontroli. Odk�d nauczy� si� j� kontrolowa�, jest kim� znacznie bardziej gro�nym, ni� tylko wojownikiem.
Sephinroth utrzymuje, �e jest nie�lubnym dzieckiem kr�la Gryphonheart'a. Rozpali�o to w niej nienawi�� do tronu Erathii, kt�ry, jak ma nadziej�, og�osi pewnego dnia swoim w�asnym. Jest jedyn� kobiet� posiadaj�c� tytu� czarnoksi�nika.     
Darkstorn przedziera� si� przez szeregi wojownik�w Nighonu na sw�j w�asny spos�b. Wyzywa� przeciwnik�w na pojedynek, a jednocze�nie potajemnie u�ywa� magii, aby zapewni� sobie zwyci�stwo. Kiedy doszed� do w�adzy ujawni� swoj� prawdziw� pot�g�. Od tamtej pory nikt nie pr�bowa� si� z nim zmierzy�.
Yog studiowa� u mag�w w Brakadzie, ale cz�ciej znajdowano go na czytaniu ksi�g o wojnie ni� o magii. Ksi��� Krewlod zaproponowa� mu stanowisko dow�dcy, pozwalaj�c mu w pe�ni wykorzysta� sw�j zapa� i wiedz� o wojnie.
To niespotykane, aby goblin zajmowa� jak�kolwiek znacz�c� pozycj�, ale Gurnisson wykazuje wrodzony talent, je�eli chodzi o katapulty, co w oczywisty spos�b w��cza go w szeregi najemnych bohater�w.
B�d�c najstarszym synem ksi�cia Boragusa, Jabarkas wie, �e pewnego dnia zostanie w�adc� Krewlod. Podobnie jak jego ojciec wyznaje zasad�, �e tylko wojna szybko prowadzi do celu.
Shiva jest najm�odsz� z sze�ciu c�rek rodziny Treser�w zwierz�t. Poniewa� perspektywa pozostania w rodzinnym biznesie niezbyt j� n�ci�a, przy��czy�a si� do wojsk Krewlod jako najemnik.
Gretchin jest nie tylko wspania�ym dow�dc�: wszyscy z jej otoczenia wci�� powtarzaj�, �e dba o swoich �o�nierzy, jak o w�asne dzieci. 
Na pocz�tku Krellion by� �owc� nagr�d, ale kiedy sta� si� znany, jego zwolennicy chcieli s�u�y� pod jego komend�. Chocia� ich wyb�r nie wydawa� si� nie by� rozs�dnym, Krellion udowodni�, �e jest wspania�ym dow�dc�, szczeg�lnie w�r�d ogr�w.
Uznanego bohatera z Enroth, Craga Hacka tak bardzo znudzi�y rodzinne okolice, �e postanowi� spr�bowa� innego �ycia w Erathii. Rozkoszuje si� my�l�, �e w ko�cu znalaz� pe�ne zamieszania miejsce, gdzie w pe�ni mo�e wykorzysta� swoje mo�liwo�ci.
Tyraxor by� kiedy� kapitanem kawalerii.  Waha� si�, czyj� stron� poprze�, lecz kiedy uda�o mu si� odkry� tatalia�sk� zasadzk� do��czy� do szereg�w bohater�w Krewlod.                   
Wszystko, co umie Gird zawdzi�cza szamanowi z rodzinnego miasteczka. W przysz�o�ci chcia�a zaj�� jego miejsce, lecz niestety zosta�a zabrana, kiedy mag z Krewlod prowadzi� poszukiwania dzieci umiej�cych w�ada� magi�.
Szaman Vey wielokrotnie udowodni�, �e jego dow�dcze zdolno�ci s� niezr�wnane. Vey z ca�� pewno�ci� b�dzie jednym z najpowa�niejszych kandydat�w na nast�pc� tronu Krewlod.
Blisko pi�tna�cie lat temu ca�a wioska Dessy zosta�a zmieciona z powierzchni ziemi przez ludzkich najemnik�w. Dessa by� jednym z sze�ciorga dzieci, kt�re prze�y�y masakr�. Od tamtej pory marzy on o nadej�ciu dnia, w kt�rym odp�aci ludziom za t� zbrodni�.
Terek sp�dzi� kilka lat w cyrku S�o�ca pracuj�c jako si�acz wzywaj�cy widz�w do zmierzenia si� z nim w rzucaniu ska�ami na odleg�o��. Pewnego dnia przegra� i zosta� wyrzucony, ale cz�owiek kt�ry go pokona�, zaci�gn�� go do armii Krewlod.
Zubin by� kiedy� wodzem plemienia walcz�cego przeciwko ksi�ciu Boragusowi. Kiedy wreszcie zapanowa� pok�j, zaproponowano mu stanowisko dow�dcy w armii Krewlod.
Osierocona w dniu narodzin, Gundula by�a wychowywana jako c�rka ksi�cia Boragusa. Kiedy dowiedzia�a si�, �e on nie jest jej prawdziwym ojcem, wst�pi�a do wojska, maj�c nadziej� na zaszczytn� �mier� na polu walki. Tak si� nie sta�o i obecnie Gundula jest bardzo powa�anym dow�dc�.
Nikt nie wierzy�, �e Oris b�dzie kiedykolwiek porz�dnym magiem, ale jej niesamowity dar uczenia si� nowych zakl�� wynagradza jej wszystkie inne braki w magicznym rzemio�le.
Kr��� plotki, �e Saurug studiowa� alchemi� tylko po to, aby sta� si� cz�owiekiem bardzo bogatym. Chocia� mu si� to nie uda�o, jego magia sprawi�a, i� on sam sta� si� bogactwem dla armii Krewlod.
Bron, chocia� jest tylko cz�owiekiem, posiada niesamowit� odporno�� na zab�jczy wzrok bazyliszka. Pozwoli�o mu to dowiedzie� si� o nim znacznie wi�cej, ni� komukolwiek innemu.
Drakon mia� opini� najlepszego wojownika w wiosce, p�niej w ca�ej okolicy, zanim wybra� s�u�b� kr�lowi Tralossk. Jego charyzmatyczna postawa sprawia�a, �e �o�nierze ch�tnie przechodzili pod jego komend�.
O�eniony z najstarsz� c�rk� kr�la Tralosska, Wystan jest dziedzicem tronu Tatalii. B�dzie s�u�y� i uczy� si� od kr�la, a� do nadej�cia w�a�ciwego momentu kiedy b�dzie pr�bowa� zabi� swojego mentora i przej�� tron.
Podczas sze�ciomiesi�cznej granicznej wojny z Krewlod, niewielka grupa dowodzona przez Tazara, zdo�a�a utrzyma� tatalia�ski posterunek, powstrzymuj�c prawie pi�ciokrotnie silniejsze wojska z Krewlod przez osiem dni, a� do czasu przybycia posi�k�w. 
Kr�l Tralossk nigdy nie darzy� sympati� Alkina, i prawd� m�wi�c uczyni� z niego dow�dc� w nadziei, �e polegnie on na polu bitwy. Tak si� jednak nie sta�o, a kariera Alkina nabiera�a coraz szybszego tempa.
"Korbac sta� si� znany mieszka�com Erathii jako ""bohater, kt�ry uratowa� ucznia Xantora"". Udowodni� on te� swoj� odwag� w wielu sytuacjach podczas s�u�by w Tatalii."
Gerwulf sprowadzi� pierwsz� balist� do Tatalii, po tym jak zdoby� j� podczas obl�enia barbarzy�skiej fortecy na granicy Krewlod. Zosta� najwi�kszym ekspertem Tatalii w jej stosowaniu na ca�ym p�nocnym zachodzie.
Dziadek Broghilda jako pierwszy poskromi� dzik� wiwern�, aby u�ywa� jej do lot�w zwiadowczych nad tatalia�skimi nizinami. Sekret panowania nad tymi stworzeniami jest przekazywany z ojca na syna, wi�c trafi� r�wnie� do Broghilda.
W przesz�o�ci Miranda igra�a z czarn� magi�, zanim zrozumia�a, �e taka droga z regu�y prowadzi do samodestrukcji. Nauczy�a si� st�pa� po w�skiej �cie�ce mi�dzy dobrem i z�em, zawsze uwa�aj�c, aby �adna z tych si� nie sta�a si� pot�niejsza od drugiej.
Kiedy Rosic by�a dzieckiem, dosta�a tak strasznej gor�czki, �e nikt ze starszyzny w wiosce nie spodziewa� si� jej wyzdrowienia. Ale prze�y�a i teraz twierdzi, �e ta gor�czka jest �r�d�em jej obecnych magicznych mocy.
"Voy jest ""wied�m� wiatru"", cz�sto zatrudnian� przez kapitan�w morskich statk�w do pomocy w nawigacji i zapewnieniu ich jednostkom sprzyjaj�cych wiatr�w. Woli s�ony, �wie�y powiew wiatru nad oceanem od zaduchu bagien, na kt�rych si� wychowa�a."
Verdish o ma�y w�os nie zgin�a podczas najazdu W�adc�w Podziemi na jej ojczyzn�. Na szcz�cie, jest mistrzyni� sztuki leczenia i by�a w stanie wyleczy� �miertelne rany zadane jej w czasie walki.
Podobno Merist by�a uzdrowicielk� w swojej mie�cinie, ale kiedy odkryto, �e jej magiczna moc jest du�o silniejsza, ni� wymaga tego stanowisko miejscowego uzdrowiciela szybko wcielono j� do armii.
Kiedy sta�o si� jasne, �e nigdy nie zasi�dzie na tronie, Styg, jedenasta c�rka Tralosska, kr�la Tatalii, zaj�a si� czarami.
Zanim kr�l Tralossk awansowa�  Andr�, zwyk�ego cz�owieka, na jednego z dow�dc�w swojej armii, podda� j� serii pr�b, kt�re mia�y sprawdzi� jej wiedz�. Poniewa� w czasie tych test�w nie pad�a ani jedna b��dna odpowied�, kr�l nie mia� innego wyboru, jak j� zaakceptowa�.
Tiva osi�gn�a pozycj� bohatera wyzywaj�c tych, kt�rzy stali w hierarchii wy�ej od niej, na pojedynki w szybko�ci nauki. Oczywi�cie, zawsze udowadnia�a, �e jest szybszym studentem, ni� jej przeciwnicy. Ma na swoim koncie wiele sukces�w.
Pasis zawsze by�a zafascynowana zmiennokszta�tnymi. Jej studia uczyni�y z niej idealn� kandydatk� na mieszka�ca wr�t �ywio��w.	
Mieszkaj�c w pobli�u stolicy Erathii Thunar nauczy�a si� zdobywa� z�oto oraz pos�ugiwa� taktyk�. Jej poszanowanie �ycia zapewni�o jej miejsce w�r�d mieszka�c�w wr�t �ywio��w.	
Cztery lata temu pot�ny mag przywo�a� Igiss� do tego �wiata. Nie maj�c szans na powr�t b��ka�a si� a� poczu�a zew w�adc�w wr�t �ywio��w.	
Trudne zawody zdecydowa�y, kt�ry z �ywio�ak�w wody otrzyma� honor udania si� do Wr�t �ywio��w. Lacus zawdzi�cza swe zwyci�stwo zdolno�ciom taktycznym.	
B�d�c istot� z�o�on� z czystej energii Monere cieszy� si� sw� postaci�. Jednak jego zainteresowanie stworzeniami posiadaj�cymi cielesn� pow�ok� przywiod�o go do wr�t �ywio��w.	
Staro�ytny pan lawy, Erdamon, przespa� wieki w�r�d g�r Eeofolu. Wezwany na pomoc, cieszy si� na bitw�, w kt�rej zmiecie Kreegan z powierzchni ziemi.	
Z racji tego �e Fiur jest bardzo m�ody, nie zd��y� jeszcze dowie�� swoich umiej�tno�ci bojowych. Niekt�rzy uwa�aj�, �e to w�a�nie on ma szans� sta� si� najwi�kszym spo�r�d mieszka�c�w wr�t �ywio��w.	
Kalf zawsze szybko si� uczy�, dlatego tak ch�tnie pos�ucha� wezwania do Wr�t �ywio��w, gdzie ma szans� pozna� to, czego nikt wcze�niej ni pozna�.	
Czarodziejka z Enroth, Luna, otrzyma�a wezwanie do Wr�t �ywio��w i uda�a si� w drog� przez Erathi�. Tam te� porzuci�a szko�� magii wody i bez reszty odda�a si� mocy ognia.	
M�ody d�inn, Brissa, przy��czy�a si� do wojen o odrodzenie Erathii zanim uko�czy�a swe szkolenia. Mimo brak�w wykszta�cenia potrafi sobie doskonale radzi� dzi�ki swojemu do�wiadczeniu.	
Wychowuj�c si� nad brzegami oceanu w AvLee, Ciele zawsze czu�a zwi�zek w wod�. Przemierzy�a niezliczone krainy szukaj�c powo�ania a� w ko�cu dotar�a do wr�t �ywio��w.	
Biedna Labetha straci�a rodzin� podczas wojen o odrodzenie Erathii. Przygarni�ta przez drzewce rozwija�a swe umiej�tno�ci bardzo szybko. Nie zwleka�a z odpowiedzi�, gdy wezwali j� panowie wr�t �ywio��w.	
W m�odo�ci Inteus popisywa� si� swymi zdolno�ciami magii ognia w grupie w�drownych artyst�w. Niekt�rzy uwa�aj� i� jest synem �ywio�aka ognia. Pewnej nocy poczu� wezwanie i od tamtej pory s�u�y panom wr�t �ywio��w.	
Mimo �e jest d�innem, Aenain czu� si� niedoskona�y. Przez wieki pr�bowa� przezwyci�y� to uczucie. Doskona�o�� i po�wi�cenie oraz znajomo�� magii powietrza zapewni�y mu miejsce w�r�d mieszka�c�w wr�t �ywio��w.	
Elf Gelare urodzi� si� z wiedz� niespotykan� nawet w�r�d jego pobratymc�w. Spokojny, pe�en zadumy nie sprzeciwia� si�, gdy wezwano go do wr�t �ywio��w.	
Krasnoludy kochaj� z�oto. Jedne mniej, inne bardziej. Ale nikt lepiej od Grindana nie zna si� na jego poszukiwaniu.	
Sir Mullich mimo spokoju i ciszy doskonale potrafi zmobilizowa� swe jednostki do niesamowicie szybkich dzia�a�.
Adrienne rzuci�a na szal� sw�j los i przysz�o��, byle tylko m�c zdobywa� moc i umiej�tno�ci niezb�dne do obrony kraju. Kosztowa�o j� to samotno�� i niezrozumienie.
Jaka panuj�ca kr�lowa Erathii Katarzyna nadal walczy o bezpiecze�stwo swych poddanych i pok�j. Ale poparcie dla jej dzia�a� spada w miar� jak jej poddani s� coraz bardziej znu�eni t� nieustaj�c� walk�.
Uwa�any za magicznego geniusza, Dracon zamierza zosta� najwi�kszym w historii pogromc� smok�w. Dlatego te� studiuje g��wnie magi� bitewn� zaniedbuj�c efektowne sztuczki czynione na pokaz gawiedzi.
Gel zosta� znaleziony i wychowany przez genera�a Morgana Kendala gdy ten s�u�y� pod Katarzyn� Ironfist. Obecnie jest przyw�dc� partyzant�w znanych jako Le�na Stra�. Pono� jest p� cz�owiekiem, p� elfem.
Od kiedy pokona� swego ojca i zaj�� jego miejsce jako przyw�dca klanu, Kilgor wci�� zdobywa s�aw� w ca�ym Krewlod. Uwa�a si�, i� ma spore szans� na wygranie Festiwalu �ycia.
Przywr�cony do �ycia przez swych wyznawc�w, lord Haart jest pot�niejszy ni� kiedykolwiek.
Posiadaj�c talent dowodzenia smokami, Mutare pnie si� coraz wy�ej w�r�d w�adc�w Nighonu.
Obecnie uwolniony od Kreegan Roland ch�tnie s�u�y jako genera� w armii umi�owanej �ony, Katarzyny. Niebawem oboje powr�c� do Enroth, jednak wcze�niej musz� zdecydowa�, co stanie si� z Erathi� i jej tronem.
Po wypiciu krwi smoka Mutare sta�a si� prawdziwym smokiem. Niekt�rzy przypuszczaj�, �e transformacja ta zapowiada nadej�cie Ojca smok�w.
Zdolno�ci Winstona Boragusa w dowodzeniu orkami zapewni�y mu powodzenie w jego pogoni za s�aw� i pot�g�.
Po pokonaniu Xenofexa, gdy Lucyfer Kreegan obj�� w�adz�, Xeron jak pierwszy z�o�y� ho�d nowemu panu. P�niej uczynili to inni bohaterowie.
Po zab�jstwie kr�la Gryphonhearta, genera� Morgan Kendal obj�� tron do czasu znalezienia nowego sukcesora. By� jedynym w�adc� Erathii, kt�ry pozwoli� na zaj�cie stolicy przez obce wojska.
Sir Christian to wci�� m�odzian. Jego ojciec nie ustaje w wysi�kach uczynienia z niego pot�nego czarnoksi�nika.
Pomimo, �e Ojciec nada� mu imi� wojownika, Ordwald odrzuci� sztuk� walki i po�wi�ci� si� magii. Jest bardzo stary jak na czarnoksi�nika.
Tylko wizerunek
Tylko wizerunek
Tylko wizerunek
Tylko wizerunek
